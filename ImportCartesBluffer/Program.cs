﻿using ImportCartesBluffer.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace ImportCartesBluffer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Créer la connexion à la base de données :
            BlufferContext context = new BlufferContext();

            //Identifier le dossier contenant toutes les images (C:\Users\Jules\Documents\Scans_Bluffer)
            string cheminDossier = String.Format("{0}\\Scans_Bluffer", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            DirectoryInfo dossier = new DirectoryInfo(cheminDossier);

            //Pour chaque fichier dans le dossier
            foreach (var fichier in dossier.GetFiles("*.JPEG"))
            {
                //Charger l'image depuis le disque dur
                Image<Rgba32> image = Image.Load(fichier.FullName);
                Console.WriteLine(fichier.Name);

                //Pour décaler selon les 9 cartes du scan
                for (int num_carte = 0; num_carte < 9; num_carte++)
                {
                    int larg_carte = 734;
                    int long_carte = 1041;
                    // modulo 3 car 3 rangs
                    double decal_X = (num_carte % 3) * 820 + 58;
                    double decal_Y = (num_carte / 3) * 1140 + 52;
                    // 88 mm = 682 pxl = longueur carte 
                    double mm_pxl = long_carte / 88.0;


                    Image<Rgba32> imageQuestion = null;
                    Image<Rgba32> imageReponse = null;
                    //Découper l'image (4 questions, 4 réponses, la moitié dans un sens, l'autre moitié dans l'autre)
                    for (int i = 0; i < 4; i++)
                    {
                        // Image<Rgba32> imageQuestion = DecouperImage(image, 39, 340, 404, 98);
                        switch (i)
                        {
                            case (0):
                                // question/réponse bord de carte (réponse puis qestion)
                                // taille fenetre réponse 11 mm  ; taille fenêtre question 14 mm 
                                imageQuestion = DecouperImage(image, 0 + decal_X, 9 * mm_pxl + decal_Y, larg_carte, 14 * mm_pxl); // 9 au lieu de 11
                                imageReponse = DecouperImage(image, 0 + decal_X, 0 + decal_Y, larg_carte, 11 * mm_pxl);
                                break;

                            case (1):
                                // question/réponse milieu de carte (réponse puis qestion)
                                // taille fenetre réponse 7 mm  ; taille fenêtre question 14 mm 
                                imageQuestion = DecouperImage(image, 0 + decal_X, 30 * mm_pxl + decal_Y, larg_carte, 14 * mm_pxl); //30 au lieu de 32
                                imageReponse = DecouperImage(image, 0 + decal_X, 23 * mm_pxl + decal_Y, larg_carte, 7 * mm_pxl); //23 au lieu de 25
                                break;

                            case (2):
                                // question/réponse bord de carte (réponse puis qestion)
                                // taille fenetre réponse 11 mm  ; taille fenêtre question 14 mm 
                                imageQuestion = DecouperImage(image, 0 + decal_X, long_carte - 25 * mm_pxl + decal_Y, larg_carte,14 * mm_pxl); //  au lieu de 25
                                imageReponse = DecouperImage(image, 0 + decal_X, long_carte - 11* mm_pxl + decal_Y, larg_carte, 11 * mm_pxl);
                                break;

                            case (3):
                                // question/réponse milieu de carte (réponse puis qestion)
                                // taille fenetre réponse 7 mm  ; taille fenêtre question 14 mm 
                                imageQuestion = DecouperImage(image, 0 + decal_X, long_carte - 45 * mm_pxl + decal_Y, larg_carte, 14 * mm_pxl); //45 au lieu de 46
                                imageReponse = DecouperImage(image, 0 + decal_X, long_carte - 32 * mm_pxl + decal_Y, larg_carte, 7 * mm_pxl);
                                break;

                            default:
                                break;
                        }
                       

                

                //Retourner une image à 180 degrés
                if (i < 2)
                        {
                            imageQuestion.Mutate(x => x.Rotate(180));
                            imageReponse.Mutate(x => x.Rotate(180));
                        }

                        //Sauvegarder la découpe pour testeré
                        imageQuestion.Save(String.Format("C:\\Users\\Jules\\Desktop\\test_bluffer\\Question{0}_carte{1}_.JPG",i,num_carte));
                        imageReponse.Save(String.Format("C:\\Users\\Jules\\Desktop\\test_bluffer\\Reponse{0}_{1}.JPG", i,num_carte));

                        //Faire appel à l'OCR pour récupérer le texte
                        //string questionLue = AppliquerOCR(imageQuestion);
                        string reponseLue = AppliquerOCR(imageReponse);
                        Console.WriteLine("reponse"+i+"_"+num_carte+" : "+ reponseLue);


                        //Remplacer les sauts de ligne par des espaces
                        //questionLue = questionLue.Replace("\r\n", " ");
                        //reponseLue = reponseLue.Replace("\r\n", " ");

                        //Sauvegarder les images dans des Streams
                        MemoryStream imgQ = new MemoryStream();
                        imageQuestion.SaveAsPng(imgQ);

                        MemoryStream imgR = new MemoryStream();
                        imageReponse.SaveAsPng(imgR);

                        ////Enregistrer la question en base de données
                        context.Add(new Question()
                        {
                            Probleme = "",
                            Reponse = reponseLue,
                            ImageQuestion = imgQ.ToArray(),
                            ImageReponse = imgR.ToArray()
                        });

                        //Sauvegarder les modifications en base de données
                        context.SaveChanges();
                    }
                }

            }
                   
            

            
        }

        /// <summary>
        /// Permet de découper une image
        /// </summary>
        /// <param name="L'image (format Rgba32)"></param>
        /// <param name="X du pixel en haut à gauche"></param>
        /// <param name="Y du pixel en haut à gauche de la zone de sélection "></param>
        /// <param name="hauteur en pixel de la zone de sélection"></param>
        /// <param name="largeur en pixel de la zone de sélection"></param>
        /// <returns></returns>
        private static Image<Rgba32> DecouperImage(Image<Rgba32> sourceImage, double sourceX, double sourceY, double sourceWidth, double sourceHeight)
        {
            Image<Rgba32> imagedecoupee = sourceImage.Clone();
            imagedecoupee.Mutate(x => x.Crop(new Rectangle((int)sourceX, (int)sourceY, (int)sourceWidth, (int)sourceHeight)));
            return imagedecoupee;
        }



        private static string AppliquerOCR(Image<Rgba32> image)
        {

            HttpClient httpClient = new HttpClient();
            httpClient.Timeout = new TimeSpan(1, 1, 1);


            MultipartFormDataContent form = new MultipartFormDataContent
            {
                { new StringContent("c55ecbd4ed88957"), "apikey" }, //Added api key in form data
                { new StringContent("fre"), "language" },
                { new StringContent("false"), "isOverlayRequired" }
            };


            if (image != null)
            {
                using (var ms = new MemoryStream())
                {
                    image.SaveAsJpeg(ms);
                    form.Add(new ByteArrayContent(ms.ToArray(), 0, ms.ToArray().Length), "image", "image.jpg");
                }

            }

            string strContent = null;
            Task.Run(async () =>
            {
                HttpResponseMessage response = await httpClient.PostAsync("https://api.ocr.space/Parse/Image", form);
                strContent = await response.Content.ReadAsStringAsync();
                // Do any async anything you need here without worry
            }).GetAwaiter().GetResult();

            JObject ocrResult = JsonConvert.DeserializeObject<JObject>(strContent);
            return ocrResult.SelectToken("$.ParsedResults[0].ParsedText").ToString();


        }

    }
}
