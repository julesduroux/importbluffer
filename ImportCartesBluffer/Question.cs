﻿using Microsoft.EntityFrameworkCore;

namespace ImportCartesBluffer.Models
{
    public class BlufferContext : DbContext
    {
        public DbSet<Question> Question { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=tcp:bluffer.database.windows.net,1433;Initial Catalog=Bluffer;Persist Security Info=False;User ID=moiazejdij;Password=oW4qhqy2P0t9GuXVWbng;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
    }

    public class Question
    {
        public int Id { get; set; }
        public string Probleme { get; set; }
        public string Reponse { get; set; }
        public byte[] ImageQuestion { get; set; }
        public byte[] ImageReponse { get; set; }
    }
}
