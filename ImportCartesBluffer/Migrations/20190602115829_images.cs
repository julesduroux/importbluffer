﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ImportCartesBluffer.Migrations
{
    public partial class images : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "imageQuestion",
                table: "Questions",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "imageReponse",
                table: "Questions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "imageQuestion",
                table: "Questions");

            migrationBuilder.DropColumn(
                name: "imageReponse",
                table: "Questions");
        }
    }
}
